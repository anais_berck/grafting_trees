# Introduction

Imagine books as transformed trees. Some live only for a year, others for ten years, some stay alive during centuries. What would a graft of one of those trees look like?  
*Grafted trees* takes quotes about trees from existing works.  
Each graft is defined by its gardener, who is presented with a short biography scraped from Wikipedia.  
The reader chooses the amount of seasons the graft will grow. A random noun is picked and defined as a 'bud' from which a new branch grows by replacing the word by its definition.  
*Grafted trees* is a book in the 'Algoliterary Publishing House: making kin with trees'. The author of this book is the [Oulipo](https://www.oulipo.net/)'s constraint of [Littérature définitionnelle](https://www.oulipo.net/fr/contraintes/litterature-definitionnelle), invented by [Marcel Bénabou](https://en.wikipedia.org/wiki/Marcel_B%C3%A9nabou) in 1966: in a given phrase, one replaces every significant element (noun, adjective, verb, adverb) by one of its definitions in a given dictionary; one reiterates the operation on the newly received phrase, and again.  

The book is by definition infinite and each copy is unique.  
This copy counts {{ xxx }} chapters that correspond to the {{ xxx }} amount of seasons the reader wanted to see the graft grow.  

*Grafted trees* is a work by [Anaïs Berck](https://www.anaisberck.be/), a pseudonym that represents a collaboration between humans, algorithms and trees. Anaïs Berck explores the specificities of human intelligence in the company of artificial and plant intelligences. 

In this work Anaïs berck is represented by:
- the constraint of [Littérature définitionnelle](https://www.oulipo.net/fr/contraintes/litterature-definitionnelle), invented by Oulipian [Marcel Bénabou](https://en.wikipedia.org/wiki/Marcel_B%C3%A9nabou) in 1966,
- the human beings who lent their quotes about trees as grafts: Maya Angelou, William Blake, Scott Blum with *Waiting for Autumn*, Tim Flannery in Peter Wohllebens *The Hidden Life of Trees*, Kahlil Gibran with *Sand and Foam*, Thomas Hardy with *Under the Greenwood Tree*, Bernd Heinrich with *The Trees in My Forest*, Hermann Hesse with *Baume. Betrachtungen und Gedichte*, Mehmet Murat Ildan, Santosh Kalwar, Andrea Koehle Jones with *The Wish Trees*, Kamand Kojouri, Laura Lafargue with *Correspondence Volume 2 1887-1890*, John Lubbock with *The Use Of Life*, Richard Mabey with *Beechcombings: The narratives of trees*, Chris Maser with *Forest Primeval: The Natural History of an Ancient Forest*, Mokokoma Mokhonoana, Vera Nazarian with *The Perpetual Calendar of Inspiration*, George Orwell, Dorothy Parker with *Not So Deep As A Well: Collected Poems*, Clarissa Pinkola Estes with *The Faithful Gardener: A Wise Tale About That Which Can Never Die*, Terry Pratchett with *Good Omens: The Nice and Accurate Prophecies of Agnes Nutter, Witch*, Marcel Proust, Jim Robbins with *The Man Who Planted Trees: Lost Groves, Champion Trees, and an Urgent Plan to Save the Planet*, Franklin D. Roosevelt, Seneca, Jodi Thomas with *Welcome to Harmony*, Ralph Waldo Emerson.
- the human beings who lent their code: Marcel Bénabou, Gijs de Heij, An Mertens.

# General description
## Oulipo constraints as authors
*An Algoliterary Publishing House: making kin with trees* finds its origins in the tradition of Oulipo (http://oulipo.net/) through the work of [Algolit](https://algolit.net), the artistic research group around free code and text based in Brussels and initiated in 2012. That is why we think the Oulipo constraints deserve a place in it as authors. Following Wikipedia, Oulipo (French pronunciation: ​[ulipo], short for French: Ouvroir de littérature potentielle; roughly translated: "workshop of potential literature", stylized OuLiPo) is a loose gathering of (mainly) French-speaking writers and mathematicians who seek to create works using constrained writing techniques. It was founded in 1960 by [Raymond Queneau](https://en.wikipedia.org/wiki/Raymond_Queneau) and [François Le Lionnais](https://en.wikipedia.org/wiki/Fran%C3%A7ois_Le_Lionnais). Other notable members have included novelists [Georges Perec](https://en.wikipedia.org/wiki/Georges_Perec) and [Italo Calvino](https://en.wikipedia.org/wiki/Italo_Calvino), poets [Oskar Pastior](https://en.wikipedia.org/wiki/Oskar_Pastior) and [Jean Lescure](https://en.wikipedia.org/wiki/Jean_Lescure), and poet/mathematician [Jacques Roubaud](https://en.wikipedia.org/wiki/Jacques_Roubaud).  
Oulipian constraints or constraint writing techniques can be compared to writing scripts of code. The instructions are clear and repetitive. A lot of them are easily trasnferrable to code. A famous example is *Cent mille milliards de poèmes* by Raymond Queneau. It is a book of ten poems cut in verses. From these verses you can create a hundred thousand billion properly-formed sonnets. Translated to an online version on [http://www.bevrowe.info/Internet/Queneau/Queneau.html](http://www.bevrowe.info/Internet/Queneau/Queneau.html).

[Algolit](https://algolit.net) was inspired by Oulipo for their first coding and algoliterary experiments. In the spirit of Oulipo (http://oulipo.net/), Algolit participants share still today their experiences in coding and writing and explore new territories during monthly meetings.
When the algoliterary writers engage in their work according to the principles of Oulipo they take up the notion of literature as the Oulipians understand it: this includes all linguistic production, from the dictionary to the Bible, from the entire work of Virginia Woolf to all the versions of Terms of Service published by Google since its existence. In this sense, code can also be literature. The participants of Algolit implement algorithms that function as constraint sequences, with the aim of being surprised, exploring and discovering instead of seeking to optimise a style or to strive for efficiency. Then a field of potential literature opens up. Not only can the results be literary, but also the code can be read as a literary creation. Indeed, the way variables and functions are named also opens a door to imaginary worlds, when they can abandon their need to serve production. 

## Littérature Définitionnelle
In the original constraint formulated by writer Marcel Bénabou, each meaningful term (noun, adjective, verb, adverb) in a given statement is replaced by one of its definitions in a given dictionary; the operation is repeated on the new statement obtained, and so on.
For this edition we chose to simplify the constraint in order to enhance the legibility. We also chose to use the metaphor of the sentence as a graft from a book. The constraint indicates the growth of the graft. in that sense, it would be more 'natural' to have a graft growth following the original constraint to the detail.

# Technical description
*Littérature définitionnelle* is an Oulipo constraint by which in a given phrase, one replaces every significant element (noun, adjective, verb, adverb) by one of its definitions in a given dictionary; one reiterates the operation on the newly received phrase, and again.  
In this book the growth is indicated in bold. A random noun is picked and defined as a 'bud' in italics. From this bud a new branch grows by replacing the word by its definition in Wordnet.   
[Wordnet](https://wordnet.princeton.edu/) is a combination of a dictionary and a thesaurus that can be read by machines. According to Wikipedia it was created in the Cognitive Science Laboratory of Princeton University starting in 1985. The project was initially funded by the US Office of Naval Research and later also by other US government agencies including DARPA, the National Science Foundation, the Disruptive Technology Office (formerly the Advanced Research and Development Activity), and REFLEX.  
*Grafted trees*'* uses Part-of-Speech to define all nouns in a specific sentence. Parts-of-Speech is a category of words: noun, verb, adjective, adverb, pronoun, preposition, conjunction, interjection, and sometimes numeral, article, or determiner. NLTK, or the Natural Language Toolkit for the programming language Python allows to parse a sentence into words. Next, it includes an pretrained algorithm that can determine the part-of-speech of each word in that sentence. A random noun is then replaced by its definition in Wordnet.  

# Code

# Credits

This book is a creation of Anaïs Berck.
In this work Anaïs Berck is represented by:
- the constraint of [Littérature définitionnelle](https://www.oulipo.net/fr/contraintes/litterature-definitionnelle), invented by Oulipian [Marcel Bénabou](https://en.wikipedia.org/wiki/Marcel_B%C3%A9nabou) in 1966,
- the human beings who lent their quotes about trees as grafts: Maya Angelou, William Blake, Scott Blum with *Waiting for Autumn*, Tim Flannery in Peter Wohllebens *The Hidden Life of Trees*, Kahlil Gibran with *Sand and Foam*, Thomas Hardy with *Under the Greenwood Tree*, Bernd Heinrich with *The Trees in My Forest*, Hermann Hesse with *Baume. Betrachtungen und Gedichte*, Mehmet Murat Ildan, Santosh Kalwar, Andrea Koehle Jones with *The Wish Trees*, Kamand Kojouri, Laura Lafargue with *Correspondence Volume 2 1887-1890*, John Lubbock with *The Use Of Life*, Richard Mabey with *Beechcombings: The narratives of trees*, Chris Maser with *Forest Primeval: The Natural History of an Ancient Forest*, Mokokoma Mokhonoana, Vera Nazarian with *The Perpetual Calendar of Inspiration*, George Orwell, Dorothy Parker with *Not So Deep As A Well: Collected Poems*, Clarissa Pinkola Estes with *The Faithful Gardener: A Wise Tale About That Which Can Never Die*, Terry Pratchett with *Good Omens: The Nice and Accurate Prophecies of Agnes Nutter, Witch*, Marcel Proust, Jim Robbins with *The Man Who Planted Trees: Lost Groves, Champion Trees, and an Urgent Plan to Save the Planet*, Franklin D. Roosevelt, Seneca, Jodi Thomas with *Welcome to Harmony*, Ralph Waldo Emerson.
- the human beings who lent their code: Marcel Bénabou, Gijs de Heij, An Mertens.

The copy of this book is unique and the print run is by definition infinite.
This copy is the XXX number of copies downloaded.

Collective terms of (re)use (CC4r), 2021
Copyleft with a difference: You are invited to copy, distribute, and modify this work under the terms of the
work under the terms of the CC4r: https://gitlab.constantvzw.org/unbound/cc4r
