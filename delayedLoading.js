class delayedLoading extends Paged.Handler {
  // this let us call the methods from the the chunker, the polisher and the caller for the rest of the script 
  constructor(chunker, polisher, caller) {
    super(chunker, polisher, caller);
  }

  beforeParsed (_) {
    document.body.appendChild(document.createTextNode('this text was inserted before the slow resolve'));  
    return new Promise(function (resolve) {
      setTimeout(function () {
        document.body.appendChild(document.createTextNode('this text was inserted before pagedjs is loaded'));  
        resolve();
      }, 2000);
    });
  }
}

Paged.registerHandlers(delayedLoading);